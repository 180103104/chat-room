package usecase

import (
	"context"

	"gitlab.com/yeldisbayev/chat-room/internal/domain"
	"gitlab.com/yeldisbayev/chat-room/internal/repository"
)

type chatInteractor struct {
	chatRepo repository.ChatRepo
}

func NewChatInteractor(chatRepo repository.ChatRepo) ChatUsecase {
	return chatInteractor{
		chatRepo,
	}

}

func (interactor chatInteractor) Create(
	ctx context.Context,
	chat domain.Chat,
) (*domain.Chat, error) {
	chatFromStore, err := interactor.chatRepo.Create(ctx, chat)

	return chatFromStore, err

}

func (interactor chatInteractor) GetMany(
	ctx context.Context,
	limit,
	offset uint32,
) ([]domain.Chat, error) {
	chats, err := interactor.chatRepo.GetMany(ctx, limit, offset)

	return chats, err

}
