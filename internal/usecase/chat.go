package usecase

import (
	"context"

	"gitlab.com/yeldisbayev/chat-room/internal/domain"
)

type ChatUsecase interface {
	Create(ctx context.Context, chat domain.Chat) (*domain.Chat, error)
	GetMany(ctx context.Context, limit, offset uint32) ([]domain.Chat, error)
}
