package usecase

import (
	"context"
	"fmt"
	"time"

	"github.com/golang-jwt/jwt"
	"gitlab.com/yeldisbayev/chat-room/internal/domain"
	"gitlab.com/yeldisbayev/chat-room/internal/repository"
)

type authInteractor struct {
	userRepo     repository.UserRepo
	verifierRepo repository.VerifierRepo
}

func NewAuthInteractor(userRepo repository.UserRepo, verifierRepo repository.VerifierRepo) Auth {
	return authInteractor{
		userRepo,
		verifierRepo,
	}

}

func (a authInteractor) SignUp(ctx context.Context, user domain.User) (*domain.User, error) {
	_, err := a.userRepo.GetByLogin(ctx, user.Email)

	if err != nil {
		user, err := a.userRepo.Create(ctx, user)

		if err != nil {
			return nil, fmt.Errorf("%w: %e", ErrCreateUser, err)
		}

		_, err = a.verifierRepo.Create(ctx, user.Email)

		if err != nil {
			return nil, fmt.Errorf("%w: %e", ErrCreateVerifier, err)
		}

		return user, nil

	}

	return nil, ErrUserAlreadyExists

}

func (a authInteractor) VerifyIdentity(ctx context.Context, verifier domain.Verifier) error {
	verifierFromStorage, err := a.verifierRepo.Get(ctx, verifier.ID)

	if err != nil {
		return fmt.Errorf("%w: %e", ErrGetVerifier, err)
	}

	if verifier.Email != verifierFromStorage.Email {
		return fmt.Errorf("%w: %e", ErrInvalidVerifier, err)
	}

	user, err := a.userRepo.GetByEmail(ctx, verifier.Email)

	if err != nil {
		return fmt.Errorf("%w: %e", ErrGetUser, err)
	}

	if user.Verified {
		return fmt.Errorf("%w: %e", ErrUserAlreadyVerified, err)
	}

	user.Verified = true

	_, err = a.userRepo.Update(ctx, *user)

	if err != nil {
		return fmt.Errorf("%w: %e", ErrVerifyUser, err)
	}

	return nil

}

func (a authInteractor) SignIn(ctx context.Context, login, password string) (string, error) {
	user, err := a.userRepo.GetByLogin(ctx, login)

	if err != nil {
		return "", fmt.Errorf("%w: %e", ErrGetUser, err)
	}

	if user.Password != password {
		return "", fmt.Errorf("%w: %e", ErrInvalidCredentials, err)
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"user_id":  user.ID,
		"username": user.Username,
		"nbf":      time.Now().Add(15 * time.Minute).Unix(),
	})

	var hmacSampleSecret []byte
	// Sign and get the complete encoded token as a string using the secret
	tokenString, err := token.SignedString(hmacSampleSecret)

	if err != nil {
		return "", err
	}

	return tokenString, nil

}
