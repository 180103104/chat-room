package usecase

import (
	"context"
	"errors"

	"gitlab.com/yeldisbayev/chat-room/internal/domain"
)

var ErrUserAlreadyExists = errors.New("user already exists")
var ErrUserAlreadyVerified = errors.New("user already verified")
var ErrCreateUser = errors.New("cannot create user")
var ErrVerifyUser = errors.New("cannot verify user")
var ErrGetUser = errors.New("cannot get user")
var ErrGetVerifier = errors.New("cannot get verifier")
var ErrCreateVerifier = errors.New("cannot create verifier")
var ErrInvalidCredentials = errors.New("login or password invalid")
var ErrInvalidVerifier = errors.New("login or password invalid")

type Auth interface {
	SignUp(ctx context.Context, user domain.User) (*domain.User, error)
	VerifyIdentity(ctx context.Context, verifier domain.Verifier) error
	SignIn(ctx context.Context, login, password string) (string, error)
}
