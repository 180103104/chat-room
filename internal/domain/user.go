package domain

type User struct {
	ID       string
	Email    string
	Username string
	Password string
	Verified bool
}

func NewUser(email, username, password string) User {
	return User{
		Email:    email,
		Username: username,
		Password: password,
	}

}
