package domain

type Verifier struct {
	ID    string
	Email string
}

func NewVerifier(id, email string) Verifier {
	return Verifier{
		id,
		email,
	}
}
