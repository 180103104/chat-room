package delivery

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/yeldisbayev/chat-room/internal/domain"
	"gitlab.com/yeldisbayev/chat-room/internal/usecase"
)

type ChatDelivery struct {
	chatUsecase usecase.ChatUsecase
}

func NewChatDelivery(chatUsecase usecase.ChatUsecase) ChatDelivery {
	return ChatDelivery{
		chatUsecase,
	}

}

type inputCreateChat struct {
	Name string `json:"name" form:"name"`
}

type inputGetMany struct {
	Limit  uint32 `json:"limit" form:"limit" binding:"required,min=20,max=100"`
	Offset uint32 `json:"offset" form:"offset"`
}

func (delivery ChatDelivery) Create(ctx *gin.Context) {
	inp := new(inputCreateChat)

	if err := ctx.BindJSON(inp); err != nil {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"Message": err.Error()})
		return
	}

	chat, err := delivery.chatUsecase.Create(ctx, domain.Chat{Name: inp.Name})

	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"Message": err.Error()})
		return
	}

	ctx.AbortWithStatusJSON(http.StatusOK, chat)

}

func (delivery ChatDelivery) GetMany(ctx *gin.Context) {
	inp := new(inputGetMany)

	if err := ctx.ShouldBindQuery(inp); err != nil {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"Message": err.Error()})
		return
	}

	chats, err := delivery.chatUsecase.GetMany(ctx, inp.Limit, inp.Offset)

	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"Message": err.Error()})
		return
	}

	ctx.AbortWithStatusJSON(http.StatusOK, chats)

}
