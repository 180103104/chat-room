package delivery

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/yeldisbayev/chat-room/internal/domain"
	"gitlab.com/yeldisbayev/chat-room/internal/usecase"
)

type AuthDelivery struct {
	authUsecase usecase.Auth
}

func NewAuthDelivery(authUsecase usecase.Auth) AuthDelivery {
	return AuthDelivery{
		authUsecase,
	}

}

type inputSignUp struct {
	Email    string `json:"email" form:"email"`
	Username string `json:"username" form:"username"`
	Password string `json:"password" form:"password"`
}

type inputSignIn struct {
	Login    string `json:"login" form:"login"`
	Password string `json:"password" form:"password"`
}

func (delivery AuthDelivery) SignUp(ctx *gin.Context) {
	inp := new(inputSignUp)

	if err := ctx.BindJSON(inp); err != nil {
		fmt.Println(err)
		ctx.Error(err)
		return
	}

	user, err := delivery.authUsecase.SignUp(ctx, domain.NewUser(inp.Email, inp.Username, inp.Password))

	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"Message": err.Error()})
		return
	}

	user.Password = ""
	fmt.Println(user)

	ctx.AbortWithStatusJSON(http.StatusOK, user)

}

func (delivery AuthDelivery) Verify(ctx *gin.Context) {
	id := ctx.Param("id")
	email := ctx.Param("email")

	err := delivery.authUsecase.VerifyIdentity(ctx, domain.NewVerifier(id, email))

	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"Message": err.Error()})
		return
	}

	ctx.AbortWithStatusJSON(http.StatusOK, gin.H{"Message": "verified"})

}

func (delivery AuthDelivery) SignIn(ctx *gin.Context) {
	inp := new(inputSignIn)
	if err := ctx.BindJSON(inp); err != nil {
		ctx.Error(err)
		return
	}

	token, err := delivery.authUsecase.SignIn(ctx, inp.Login, inp.Password)

	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"Message": err.Error()})
		return
	}

	ctx.AbortWithStatusJSON(http.StatusOK, gin.H{"Token": token})

}
