package repository

import (
	"context"
	"errors"

	"gitlab.com/yeldisbayev/chat-room/internal/domain"
	"gitlab.com/yeldisbayev/chat-room/pkg/mongohelper"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type userMongoRepo struct {
	collection mongohelper.Collection
}

func NewUserRepo(collection mongohelper.Collection) UserRepo {
	return userMongoRepo{
		collection,
	}

}

type userModel struct {
	ID       primitive.ObjectID `bson:"_id"`
	Email    string             `bson:"email"`
	Username string             `bson:"username"`
	Password string             `bson:"password"`
	Verified bool               `bson:"verified"`
}

func (u userMongoRepo) Create(ctx context.Context, user domain.User) (*domain.User, error) {
	userModel := userModel{
		ID:       primitive.NewObjectID(),
		Email:    user.Email,
		Username: user.Username,
		Password: user.Password,
	}

	result, err := u.collection.InsertOne(ctx, userModel)

	if err != nil {
		return nil, err
	}

	if oid, ok := result.InsertedID.(primitive.ObjectID); ok {
		user.ID = oid.Hex()
	}

	return &user, nil

}

func (u userMongoRepo) GetByEmail(ctx context.Context, email string) (*domain.User, error) {
	result := u.collection.FindOne(ctx, primitive.M{"email": email})

	userModel := userModel{}

	if err := result.Decode(&userModel); err != nil {
		return nil, err
	}

	user := domain.User{
		ID:       userModel.ID.Hex(),
		Email:    userModel.Email,
		Username: userModel.Username,
		Verified: userModel.Verified,
	}

	return &user, nil

}

func (u userMongoRepo) GetByLogin(ctx context.Context, login string) (*domain.User, error) {
	filter := primitive.M{
		"$or": []primitive.M{
			{
				"email": login,
			},
			{
				"username": login,
			},
		},
	}

	result := u.collection.FindOne(ctx, filter)

	userModel := userModel{}

	if err := result.Decode(&userModel); err != nil {
		return nil, err
	}

	user := domain.User{
		ID:       userModel.ID.Hex(),
		Email:    userModel.Email,
		Username: userModel.Username,
		Password: userModel.Password,
		Verified: userModel.Verified,
	}

	return &user, nil

}

func (u userMongoRepo) Update(ctx context.Context, user domain.User) (*domain.User, error) {
	objectID, err := primitive.ObjectIDFromHex(user.ID)

	if err != nil {
		return nil, err
	}

	filter := primitive.M{
		"_id": objectID,
	}

	update := primitive.M{
		"$set": primitive.M{
			"email":    user.Email,
			"username": user.Username,
			"verified": user.Verified,
		},
	}

	result, err := u.collection.UpdateOne(
		ctx,
		filter,
		update,
	)

	if err != nil {
		return nil, err
	}

	if result.ModifiedCount == 0 {
		return nil, errors.New("cannot find users")
	}

	return &user, nil

}
