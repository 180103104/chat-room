package repository

import (
	"context"

	"gitlab.com/yeldisbayev/chat-room/internal/domain"
)

type UserRepo interface {
	Create(ctx context.Context, user domain.User) (*domain.User, error)
	GetByEmail(ctx context.Context, email string) (*domain.User, error)
	GetByLogin(ctx context.Context, login string) (*domain.User, error)
	Update(ctx context.Context, user domain.User) (*domain.User, error)
}
