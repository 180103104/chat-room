package repository

import (
	"context"

	"gitlab.com/yeldisbayev/chat-room/internal/domain"
	"gitlab.com/yeldisbayev/chat-room/pkg/mongohelper"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type verifierMongoRepo struct {
	collection mongohelper.Collection
}

func NewVerifierRepo(collection mongohelper.Collection) VerifierRepo {
	return verifierMongoRepo{
		collection,
	}

}

type verifierModel struct {
	ID    primitive.ObjectID `bson:"_id"`
	Email string             `bson:"email"`
}

func (repo verifierMongoRepo) Create(ctx context.Context, email string) (*domain.Verifier, error) {
	verifierModel := verifierModel{
		ID:    primitive.NewObjectID(),
		Email: email,
	}

	result, err := repo.collection.InsertOne(ctx, verifierModel)

	if err != nil {
		return nil, err
	}

	verifier := domain.Verifier{
		Email: verifierModel.Email,
	}

	if oid, ok := result.InsertedID.(primitive.ObjectID); ok {
		verifier.ID = oid.Hex()

	}

	return &verifier, nil

}

func (repo verifierMongoRepo) Get(ctx context.Context, id string) (*domain.Verifier, error) {
	oid, err := primitive.ObjectIDFromHex(id)

	if err != nil {
		return nil, err
	}

	result := repo.collection.FindOne(ctx, primitive.M{"_id": oid})

	verifierModel := verifierModel{}

	if err := result.Decode(&verifierModel); err != nil {
		return nil, err
	}

	verifier := domain.Verifier{
		ID:    verifierModel.ID.Hex(),
		Email: verifierModel.Email,
	}

	return &verifier, nil

}
