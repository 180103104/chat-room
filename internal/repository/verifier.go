package repository

import (
	"context"

	"gitlab.com/yeldisbayev/chat-room/internal/domain"
)

type VerifierRepo interface {
	Create(ctx context.Context, email string) (*domain.Verifier, error)
	Get(ctx context.Context, id string) (*domain.Verifier, error)
}
