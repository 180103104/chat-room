package repository

import (
	"context"

	"gitlab.com/yeldisbayev/chat-room/internal/domain"
	"gitlab.com/yeldisbayev/chat-room/pkg/mongohelper"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type chatMongoRepo struct {
	collection mongohelper.Collection
}

func NewChatMongoRepo(collection mongohelper.Collection) ChatRepo {
	return chatMongoRepo{
		collection,
	}

}

type chatModel struct {
	ID   primitive.ObjectID `bson:"_id"`
	Name string             `bson:"name"`
}

func (repo chatMongoRepo) Create(
	ctx context.Context,
	chat domain.Chat,
) (*domain.Chat, error) {

	chatModel := chatModel{
		ID:   primitive.NewObjectID(),
		Name: chat.Name,
	}

	result, err := repo.collection.InsertOne(ctx, chatModel)

	if err != nil {
		return nil, err
	}

	if oid, ok := result.InsertedID.(primitive.ObjectID); ok {
		chat.ID = oid.Hex()
	}

	return &chat, nil

}

func (repo chatMongoRepo) GetMany(
	ctx context.Context,
	limit,
	offset uint32,
) ([]domain.Chat, error) {

	bsonLimit := int64(limit)
	bsonOffset := int64(offset)

	findoptions := options.Find()
	findoptions.Limit = &bsonLimit
	findoptions.Skip = &bsonOffset
	findoptions.Sort = bson.M{"name": 1}

	cur, err := repo.collection.Find(ctx, nil, findoptions)

	if err != nil {
		return nil, err
	}

	defer cur.Close(context.Background())

	chats := make([]domain.Chat, 0, limit)

	for cur.Next(context.Background()) {
		chatModel := chatModel{}

		err := cur.Decode(&chatModel)

		if err != nil {
			return nil, err
		}

		chat := domain.Chat{
			ID:   chatModel.ID.Hex(),
			Name: chatModel.Name,
		}

		chats = append(chats, chat)

	}

	return chats, nil

}
