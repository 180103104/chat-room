package mongohelper

import (
	"context"
)

type Cursor interface {
	ID() int64
	All(ctx context.Context, results interface{}) error
	Next(ctx context.Context) bool
	TryNext(ctx context.Context) bool
	Decode(val interface{}) error
	Close(ctx context.Context) error
	RemainingBatchLength() int
	Err() error
}
