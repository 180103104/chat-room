package mongohelper

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

type Client interface {
	Ping(ctx context.Context, rp *readpref.ReadPref) error
}

type client struct {
	*mongo.Client
}

func NewClient(mongoClient *mongo.Client) *client {
	return &client{
		mongoClient,
	}

}
