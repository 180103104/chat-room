package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/yeldisbayev/chat-room/internal/delivery"
	"gitlab.com/yeldisbayev/chat-room/internal/repository"
	"gitlab.com/yeldisbayev/chat-room/internal/usecase"
	"gitlab.com/yeldisbayev/chat-room/pkg/mongohelper"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {
	mongoDBUsername := os.Getenv("MONGODB_USERNAME")
	if mongoDBUsername == "" {
		panic("empty env MONGODB_USERNAME")
	}

	mongoDBPassword := os.Getenv("MONGODB_PASSWORD")
	if mongoDBPassword == "" {
		panic("empty env MONGODB_PASSWORD")
	}

	mongoDBServer := os.Getenv("MONGODB_SERVER")
	if mongoDBServer == "" {
		panic("empty env MONGODB_SERVER")
	}

	mongoDBPort := os.Getenv("MONGODB_PORT")
	if mongoDBPort == "" {
		panic("empty env MONGODB_PORT")
	}

	// Set client options
	clientOptions := options.Client().ApplyURI(
		fmt.Sprintf(
			"mongodb://%s:%s@%s:%s",
			mongoDBUsername,
			mongoDBPassword,
			mongoDBServer,
			mongoDBPort,
		),
	)
	// init client
	client, err := mongo.NewClient(clientOptions)

	if err != nil {
		panic(err)
	}

	if err := client.Connect(context.Background()); err != nil {
		panic(err)
	}

	usersCollection := client.Database("chat-room").Collection("users")

	verifierCollection := client.Database("chat-room").Collection("verifier")

	chatsCollection := client.Database("chat-room").Collection("chats")

	userRepo := repository.NewUserRepo(mongohelper.NewCollection(usersCollection))
	verifierRepo := repository.NewVerifierRepo(mongohelper.NewCollection(verifierCollection))
	chatRepo := repository.NewChatMongoRepo(mongohelper.NewCollection(chatsCollection))
	authUsecase := usecase.NewAuthInteractor(userRepo, verifierRepo)
	chatUsecase := usecase.NewChatInteractor(chatRepo)

	authDelivery := delivery.NewAuthDelivery(authUsecase)
	chatDelivery := delivery.NewChatDelivery(chatUsecase)

	router := gin.New()

	router.POST("/sign-up", authDelivery.SignUp)
	router.GET("/verify-identity/:id/:email", authDelivery.Verify)
	router.POST("/sign-in", authDelivery.SignIn)
	router.POST("/chats", chatDelivery.Create)
	router.GET("/chats", chatDelivery.GetMany)

	srv := &http.Server{
		Addr:    ":8080",
		Handler: router,
	}

	go func() {
		// start http server
		if err := srv.ListenAndServe(); err != nil {
			fmt.Println("Server start error: ", err)
		}

		fmt.Println("Server has been started!")

	}()

	// create channel to listening interruption
	quit := make(chan os.Signal, 1)

	// use channel to listening interruption
	signal.Notify(quit, os.Interrupt, syscall.SIGTERM)

	// wait interruption signal
	<-quit

	fmt.Println("Shutdown Server...")

	// create context with timeout
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	// shutdown server after timeout
	if err := srv.Shutdown(ctx); err != nil {
		fmt.Println("Server Shutdown:", err)
	}

}
